import '@babel/polyfill'
import 'mutationobserver-shim'
import { createApp } from 'vue'
import App from './App.vue'

import "bootstrap"

const app = createApp(App);
app.mount('#app');